#!/bin/bash

# Variables
resource_group="OCC_ASD_Lucas"
location="francecentral"
vm_name="docker-lucas"
vm_size="Standard_B2s"
admin_user="lucas"
admin_password="azerty"
virtual_network="myVNet"
subnet_name="admin"

# Créer une machine virtuelle
az vm create \
  --resource-group $resource_group \
  --name $vm_name \
  --image 'Debian11' \
  --size $vm_size \
  --admin-username $admin_user \
  --admin-password $admin_password \
  --vnet-name $virtual_network \
  --subnet $subnet_name
