#!/bin/bash

# Nom du groupe de ressources
resource_group="OCC_ASD_Lucas"

# Nom du réseau virtuel
vnet_name="myVNet"

# Emplacement (France Central)
location="francecentral"

# Adresse CIDR du réseau principal
vnet_address_prefix="10.0.8.0/24"

# Création du réseau virtuel
az network vnet create \
    --resource-group $resource_group \
    --name $vnet_name \
    --location $location \
    --address-prefix $vnet_address_prefix

# Création du sous-réseau "admin"
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $vnet_name \
    --name admin \
    --address-prefix 10.0.8.0/28

# Création des sous-réseaux supplémentaires
for (( i=0; i<=255; i+=16 ))
do
    subnet_address="10.0.8.$i/28"
    subnet_name="subnet_$i"

    az network vnet subnet create \
        --resource-group $resource_group \
        --vnet-name $vnet_name \
        --name $subnet_name \
        --address-prefix $subnet_address
done
